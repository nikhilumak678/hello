import java.util.Scanner;

class ForLoopDemo3{
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter size of array");
        int size = sc1.nextInt();
        double[] arr1 = new double[size];
        System.out.println("enter values in array");
        int a;
        for (a = arr1.length-1; a >= 0; a--) {
            arr1[a] = sc1.nextInt();
        }
//        for (a = arr1.length-1; a >= 0; a--){
//            System.out.println(arr1[a]);
//        }
//        for(a=0;a<arr1.length;a++){
//            System.out.println(arr1[a]);
//        }
        for(double i:arr1){
            System.out.println(i);
        }
    }
}