package FibonacciSeries;

public class _20 {
    static int n1=0;
    static int n2=1;

    public static void main(String[] args) {
        fab(0);//starting point
    }
    static void fab(int a){
        if (a<=20) //ending point
        {
            System.out.println(n1);//0
            int sum=n1+n2;//0+1=1
            n1=n2;//n1=1
            n2=sum;//n2=1
            a++;//a+1=2  updation point
            fab(a);//2 goes to 2<=10 yes then repeat
        }

    }
}
