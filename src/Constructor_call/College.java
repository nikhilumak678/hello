package Constructor_call;

public class College extends University{

    College(String universityName, String collegeName) {
        super(universityName);
        System.out.println("College Name="+collegeName);
    }
}
