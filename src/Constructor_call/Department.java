package Constructor_call;

public class Department extends College {

    Department(String universityName, String collegeName, String departmentName) {
        super(universityName, collegeName);
        System.out.println("Department Name="+departmentName);
    }
}
