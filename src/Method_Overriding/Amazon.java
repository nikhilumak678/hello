package Method_Overriding;

public class Amazon extends Ecommerse{
    void sellProduct(int qty, double price){
        double total= qty * price;
        double result=total-total*0.05;
        System.out.println(result+"with 5% disc");
    }
}
