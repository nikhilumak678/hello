package Method_Overriding;

import java.util.Scanner;

public class _1 {
    public static void main(String[] args) {
        Scanner sc1= new Scanner(System.in);
        System.out.println("Enter quantity");
        int qty=sc1.nextInt();
        System.out.println("Enter Price");
        double price= sc1.nextDouble();
        System.out.println("1. Flipkart");
        System.out.println("2. Amazon");
        System.out.println("Enter Choice");
        int choice= sc1.nextInt();

        if (choice==1){
            Flipkart f1= new Flipkart();
            f1.sellProduct(qty,price);
        } else if (choice==2) {
            Amazon a1=new Amazon();
            a1.sellProduct(qty,price);
        }else {
            System.out.println("Invalid input");
        }
    }
}
