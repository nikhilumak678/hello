package strings;

import javax.jws.soap.SOAPBinding;

public class _1 {
    public static void main(String[] args) {
        String s="SOFTWARE DEVELOPER";
        System.out.println(s.length());
        System.out.println(s.charAt(12));
        System.out.println(s.indexOf('W'));
        System.out.println(s.lastIndexOf('E'));
        System.out.println(s.contains("EVE"));
        System.out.println(s.startsWith("SOFT"));
        System.out.println(s.endsWith("PER"));
        System.out.println(s.substring(2));
        System.out.println(s.substring(3,9));
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());

        char[] data=s.toCharArray();
        for (int i=0;i< data.length;i++){
            System.out.println(data[i]);
        }

        String[] data1=s.split("E");
        for (int i=0;i< data1.length;i++){
            System.out.println(data1[i]);
        }

        String name="    NIK";
        System.out.println(name.trim());


    }
}
