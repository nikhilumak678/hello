import java.util.Scanner;

class ForLoopDemo4{
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter size of array");
        int size=sc1.nextInt();
        int[] data=new int[size];
        System.out.println("enter no");
        for(int a=0;a< data.length;a++){
            data[a]= sc1.nextInt();
        }
        for(int a=0;a< data.length;a++){
            System.out.print(data[a]+"\t");
        }
    }
}
