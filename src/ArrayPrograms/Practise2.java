package ArrayPrograms;

public class Practise2 {
    public static void main(String[] args) {
        int[] arr1=new int[3];
        arr1[0]=10;
        arr1[1]=20;
        arr1[2]=30;

        int[] arr2=new int[3];
        arr2[0]=40;
        arr2[1]=50;
        arr2[2]=60;

        int[] result=new int[arr1.length+ arr2.length];
        for (int i=0;i< arr1.length;i++){
            result[i]=arr1[i];
        }
        for (int i=0;i< arr1.length;i++){
            System.out.print(result[i]+"\t");
        }
        for (int i=0;i< arr1.length;i++){
            result[i]=arr2[i];
        }

        for (int i=0;i< arr1.length;i++){
            System.out.print(result[i]+"\t");
        }
    }
}
