package ArrayPrograms;
import java.util.Scanner;

public class ArrayAddition2D{
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter rows of array");
        int size=sc1.nextInt();
        System.out.println("enter columns of array");
        int size2=sc1.nextInt();
        int[][] data=new int[size][size2];
        System.out.println("enter total "+size*size2+" values");
        System.out.println("enter no");
        for(int a=0;a<size;a++){
            for (int b=0;b<size2;b++) {
                data[a][b] = sc1.nextInt();
            }
        }
        for(int a=0;a< size;a++){
            for (int b=0;b<size2;b++) {
                System.out.print(data[a][b] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("----------------");
        int[][] data1=new int[size][size2];
        System.out.println("enter total "+size*size2+" values");
        System.out.println("enter no");
        for(int a=0;a< size;a++){
            for (int b=0;b<size2;b++) {
                data1[a][b] = sc1.nextInt();
            }
        }
        for(int a=0;a< size;a++){
            for (int b=0;b< size2;b++) {
                System.out.print(data1[a][b] + "\t");
            }
            System.out.println();
        }
        int[][] sum=new int[size][size2];
        for (int a=0;a< size;a++){
            for (int b=0;b< size2;b++) {
                sum[a][b] = data1[a][b] + data[a][b];
            }
        }
        System.out.println();
        System.out.println("--------------");
        System.out.println("sum of arrays");
        for(int a=0;a< size;a++) {
            for (int b=0;b< size2;b++) {
                System.out.print(sum[a][b] + "\t");
            }
            System.out.println();
        }
    }
}
