package ArrayPrograms;

import java.util.Scanner;

public class Array2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the floors");
        int floors= sc1.nextInt();
        System.out.println("Enter the flats");
        int flats= sc1.nextInt();
        int[][] data=new int[floors][flats];
        System.out.println("Enter total of "+flats*floors+" flats");
        for (int i=0;i<floors;i++){
            for (int j=0;j<flats;j++){
                data[i][j]= sc1.nextInt();
            }
        }
        System.out.println("-----------");
        for (int i=0;i<floors;i++){
            System.out.println();
            System.out.println("floor No "+(i+1));
            System.out.println("------------------------");
            for (int j=0;j<flats;j++){
                System.out.print("flats No "+data[i][j]+"\t");
            }
            System.out.println();
            System.out.println("------------------------");
        }
    }
}
