package ArrayPrograms;
import java.util.Scanner;

public class Array1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the size of array");
        int size= sc1.nextInt();
        System.out.println("Enter the arrays values");
        int[] data=new int[size];
        for (int i=0;i<size;i++){
            data[i]= sc1.nextInt();
        }
        for (int i=0;i<size;i++){
            System.out.print(data[i]+"\t");
        }
    }
}
