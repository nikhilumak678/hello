package ArrayPrograms;

import java.util.Scanner;

public class Practise1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        int[] data=new int[4];
        data[0]=11;
        data[1]=22;
        data[2]=33;
        data[3]=44;
        System.out.println("Find position of element");
        int num = sc1.nextInt();
        for (int i=0;i< data.length;i++){
            if (num==data[i]){
                System.out.println(i+1);
                break;
            }else {
                System.out.print("");
            }
        }
    }
}
