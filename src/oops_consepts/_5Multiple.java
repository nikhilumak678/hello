package oops_consepts;
//Hierarchical Inheritance
public class _5Multiple {
    public static void main(String[] args) {
        System.out.println("Employee Details");
        PermanantEmp p1=new PermanantEmp();
        p1.getInfo(201,50000);
        p1.perEmp("SDE");
        ContractEmp c1=new ContractEmp();
        c1.getInfo(101,40000);
        c1.conEmp(5);
    }
}
