import java.net.Socket;
import java.util.Scanner;

public class ForLoopDemo2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter size of array");
        int size=sc1.nextInt();
        int[] data=new int[size];
        System.out.println("enter values");
        for (int a=0;a< data.length;a++){
            data[a]= sc1.nextInt();
        }
        int sum=0;
        int sum1=0;
        for(int a=0;a< data.length;a++){
            sum+=data[a];
            sum1-=data[a];
        }
        System.out.println(sum);
        System.out.println(sum1-10);
    }
}
