package patterns;

public class _11 {
    public static void main(String[] args) {
        int lines = 5;
        int star = 5;
        int ch = 1;
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < star; j++) {
                if (i==0||j==0||i==4||j==4) {
                    System.out.print("\t");
                } else {
                    System.out.print(ch+"\t");
                }
            }
            System.out.println();
            ch++;
        }
    }
}