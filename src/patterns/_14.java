package patterns;

public class _14 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        int ch1=1;
        for (int i=0;i<lines;i++){
            for (int j=0;j<star;j++){
                System.out.print(ch1++ +"\t");
                if (ch1>7){
                    ch1=1;
                }
            }
            System.out.println();
        }
    }
}
