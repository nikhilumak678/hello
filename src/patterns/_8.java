package patterns;

public class _8
{
    public static void main(String[] args)
    {
        int rows =5;
        int col =5;
        for (int i=0;i<rows;i++)
        {
            for (int j=0;j<col;j++)
            {
                if (i==0||j==0||i==3||j==3)
                {
                    System.out.print("\t");
                }
                else
                {
                    System.out.print("&\t");
                }
            }
            System.out.println();
        }
    }
}

