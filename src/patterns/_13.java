package patterns;

public class _13 {
    public static void main(String[] args) {
        int lines=4;
        int star=4;
        int ch1=1;
        for (int i=0;i<lines;i++){//for lines ch
            int ch2=1;
            for (int j=0;j<star;j++){ //for stars ch2
                System.out.print(ch2++ * ch1 +"\t");
            }
            System.out.println();
            ch1++;
        }
    }
}
