package patterns;

public class _16 {
    public static void main(String[] args) {
        int lines = 5;
        int star = 4;
        int ch1 = 1;
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < star; j++) {
                if (ch1%2==0){
                    System.out.print("&\t");
                }else{
                    System.out.print(ch1 +"\t");
                }
            }
            System.out.println();
            ch1++;
        }
    }
}
