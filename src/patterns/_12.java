package patterns;

public class _12 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        int ch=1;
        for (int i=0;i<lines;i++){
            int ch2=ch;
            for (int j=0;j<star;j++){
                System.out.print(ch2++ +"\t");
            }
            System.out.println();
            ch++;
        }
}
}
