package patterns;

public class _9 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        int ch=1;
        for (int i=0;i<lines;i++){
            for (int j=0;j<star;j++){
                System.out.print(ch+"\t");
            }
            System.out.println();
            ch++;
        }
    }
}