package patterns;

public class _35 {
    public static void main(String[] args) {
        int line=7;
        int star=1;
        int space=0;
        int ch1=3;
        for (int i=0;i<line;i++){
            int ch2=ch1;
            for (int k=0;k<space;k++){
                System.out.print(" ");
            }
            for (int j=0;j<star;j++){
                System.out.print(ch2++);
            }
            System.out.println();
            if (i<3){
                star++;
                ch1--;
            }else {
                star--;
                ch1++;
            }
        }


    }
}
