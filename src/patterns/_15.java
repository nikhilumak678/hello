package patterns;

public class _15 {
    public static void main(String[] args) {
        int lines=5;
        int star=4;
        char ch1='A';
        for (int i=0;i<lines;i++){
            for (int j=0;j<star;j++){
                System.out.print(ch1++ +"\t");
                if (ch1>'N'){
                    ch1='A';
                }
            }
            System.out.println();
        }
    }
}
