package patterns;

public class _10 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        int ch=1;
        for (int i=0;i<lines;i++){
            for (int j=0;j<star;j++){
                if (i==j||i+j==4||i==2||j==2){
                    System.out.print(ch+"\t");
                }else{
                    System.out.print("\t");
                }
            }
            System.out.println();
            ch++;
        }
    }
}
