package patterns;

public class _31 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        int space=4;
        int ch1=1;
        int a=1;
        for (int i=0;i<line;i++){
            for (int k=0;k<space;k++){
                System.out.print(" ");
            }
            for (int j=0;j<star;j++){
                if (j==i){
                    System.out.print(ch1+" ");
                }else {
                    System.out.print("* ");
                }

            }
            System.out.println();
            ch1++;
            star+=2;
            space--;
        }}}