package patterns;

public class _38 {
    public static void main(String[] args) {
        int line=4;
        int star=1;
        int space=3;
        int ch1=0;
        int j;
        for (int i=0;i<line;i++){
            int ch2=ch1;
            for (int k=0;k<space;k++){
                System.out.print(" ");
            }
            for (j=0;j<star;j++){
                System.out.print(ch2--+" ");
                if (j==i){
                    ch2=0;
                    ch2++;
                }
            }
            System.out.println();
            star+=2;
            space--;
            if (i>j){
                ch1--;
            }else {
                ch1++;
            }

        }


    }
}
